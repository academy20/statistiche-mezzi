package it.prismaprogetti.academy.statistichemezzi.error;

public class DecoderReaderException extends Exception{

	
	private DecoderReaderException(String message) {
		super(message);
	}
	
	
	public static DecoderReaderException isNotDirectory() {
		return new DecoderReaderException("Non � una Directory");
	}
}
