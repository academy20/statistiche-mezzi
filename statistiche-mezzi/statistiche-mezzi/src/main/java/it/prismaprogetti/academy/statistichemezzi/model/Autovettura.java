package it.prismaprogetti.academy.statistichemezzi.model;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;

/**
 * bean autovettura che estende Veicolo
 * 
 * @author alessandro.izzo
 *
 */
public class Autovettura extends Veicolo {

	private String modello;
	private String cilindrata;
	private String kw;
	private String posti;

	private Autovettura(TipoVeicolo tipologiaVeicolo, String targa, String modello, String cilindrata, String kw,
			String posti, String dataImmatricolazione) {
		super(tipologiaVeicolo, dataImmatricolazione, targa);
		this.modello = modello;
		this.cilindrata = cilindrata;
		this.kw = kw;
		this.posti = posti;

	}

	public String getModello() {
		return modello;
	}

	public String getCilindrata() {
		return cilindrata;
	}

	public String getKw() {
		return kw;
	}

	public String getPosti() {
		return posti;
	}

	public static Autovettura creaAutovettura(TipoVeicolo tipologiaVeicolo, String targa, String modello,
			String cilindrata, String kw, String posti, String dataImmatricolazione) {
		return new Autovettura(tipologiaVeicolo, targa, modello, cilindrata, kw, posti, dataImmatricolazione);
	}

	@Override
	public String toString() {
		return "Autovettura [modello=" + modello + ", cilindrata=" + cilindrata + ", kw=" + kw + ", posti=" + posti
				+ "]";
	}

}
