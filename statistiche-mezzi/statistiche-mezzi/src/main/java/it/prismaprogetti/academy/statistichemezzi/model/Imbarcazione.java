package it.prismaprogetti.academy.statistichemezzi.model;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;


/**
 * bean imbarcazione che estende Veicolo
 * @author alessandro.izzo
 *
 */
public class Imbarcazione extends Veicolo{

	private TipoVeicolo tipoVeicolo;
	private String targa;
	private String nome;
	private String velocitaMassimaKN;
	private String massimoCarico;
	private String dataImmatricolazione;

	private Imbarcazione(TipoVeicolo tipoVeicolo, String targa, String nome, String velocitaMassimaKN,
			String massimoCarico, String dataImmatricolazione) {
		super(tipoVeicolo,dataImmatricolazione,targa);
		this.tipoVeicolo = tipoVeicolo;
		this.targa = targa;
		this.nome = nome;
		this.velocitaMassimaKN = velocitaMassimaKN;
		this.massimoCarico = massimoCarico;
		this.dataImmatricolazione = dataImmatricolazione;
	}

	public TipoVeicolo getTipoVeicolo() {
		return tipoVeicolo;
	}

	public String getTarga() {
		return targa;
	}

	public String getNome() {
		return nome;
	}

	public String getVelocitaMassimaKN() {
		return velocitaMassimaKN;
	}

	public String getMassimoCarico() {
		return massimoCarico;
	}

	public String getDataImmatricolazione() {
		return dataImmatricolazione;
	}

	public static Imbarcazione creaImbarcazione(TipoVeicolo tipoVeicolo, String targa, String nome,
			String velocitaMassimaKN, String massimoCarico, String dataImmatricolazione) {

		return new Imbarcazione(tipoVeicolo, targa, nome, velocitaMassimaKN, massimoCarico, dataImmatricolazione);
	}

	@Override
	public String toString() {
		return "Imbarcazione [tipoVeicolo=" + tipoVeicolo + ", targa=" + targa + ", nome=" + nome
				+ ", velocitaMassimaKN=" + velocitaMassimaKN + ", massimoCarico=" + massimoCarico
				+ ", dataImmatricolazione=" + dataImmatricolazione + "]";
	}
	
	
}
