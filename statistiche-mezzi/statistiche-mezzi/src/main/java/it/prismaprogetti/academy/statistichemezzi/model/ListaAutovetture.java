package it.prismaprogetti.academy.statistichemezzi.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.List;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;

/**
 * classe che contiene la lista di tutte le autovetture prese dalla classe
 * ListaVeicoli
 * 
 * @author alessandro.izzo
 *
 */
public class ListaAutovetture {

	private List<Autovettura> listaAutovetture = new ArrayList<>();

	public ListaAutovetture(ListaVeicoli listaVeicoli) {
		caricaAutovetture(listaVeicoli);
	}

	private void caricaAutovetture(ListaVeicoli listaVeicoli) {

		for (Veicolo veicolo : listaVeicoli.listaVeicoli) {
			if (TipoVeicolo.A.equals(veicolo.getTipoVeicolo())) {
				listaAutovetture.add((Autovettura) veicolo);
			}
		}
	}

	public List<Autovettura> getListaAutovetture() {
		return listaAutovetture;
	}

	/**
	 * patrizio
	 * @return
	 */
	public String getImmatricolazionePiuRecente() { // [ 2014, 2018 ,2016 ]

		/*
		 *Alessandro 
		 *nel caso in cui la lista � vuota, quindi non ho date immatricolazioni da cercare
		 */
		if(listaAutovetture.isEmpty()) {
			return "Immatricolazione pi� recente: nessuna";
		}
		
		LocalDate dataImmatricolazionePiuRecente = LocalDate.of(1900, 1, 1); // 0 --> 2014 ---> 2018
		String modelloAutoPiuRecente = null;
		String targaAutoPiuRecente = null;

		for (Autovettura autovettura : this.listaAutovetture) {

			/*
			 * recupero i dati in formato String
			 */
			String dataImmatricolazioneAutoCorrenteString = autovettura.getDataImmatricolazione(); // "20160504"
			String modelloAutoCorrente = autovettura.getModello();
			String targaAutoCorrente = autovettura.getTarga();

			/*
			 * trasformo i dati relativi alla data immatricolazione da String in Integer
			 */
			Integer anno = Integer.parseInt(dataImmatricolazioneAutoCorrenteString.substring(0, 4));
			Integer mese = Integer.parseInt(dataImmatricolazioneAutoCorrenteString.substring(4, 6));
			Integer giorno = Integer.parseInt(dataImmatricolazioneAutoCorrenteString.substring(6, 8));

			LocalDate dataImmatricolazioneAutoCorrente = LocalDate.of(anno, mese, giorno);

			if (dataImmatricolazioneAutoCorrente.isAfter(dataImmatricolazionePiuRecente)) {

				dataImmatricolazionePiuRecente = dataImmatricolazioneAutoCorrente;
				modelloAutoPiuRecente = modelloAutoCorrente;
				targaAutoPiuRecente = targaAutoCorrente;
			}

			if (dataImmatricolazioneAutoCorrente.isEqual(dataImmatricolazionePiuRecente)
					&& (targaAutoCorrente.compareTo(targaAutoPiuRecente) < 0)) {

				modelloAutoPiuRecente = modelloAutoCorrente;
				targaAutoPiuRecente = targaAutoCorrente;

			}

		}

		return "Immatricolazione pi� recente: "
				+ dataImmatricolazionePiuRecente.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + " ("
				+ modelloAutoPiuRecente + " - " + targaAutoPiuRecente + ")";
	}

}
