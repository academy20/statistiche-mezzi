package it.prismaprogetti.academy.statistichemezzi.model;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;

/**
 * classe che contiene la lista di tutte le imbarcazioni prese dalla classe
 * ListaVeicoli
 * 
 * @author alessandro.izzo
 *
 */
public class ListaImbarcazioni {

	private List<Imbarcazione> listaImbarcazioni = new ArrayList<>();

	public ListaImbarcazioni(ListaVeicoli listaVeicoli) {
		caricaImbarcazioni(listaVeicoli);
	}

	private void caricaImbarcazioni(ListaVeicoli listaVeicoli) {

		for (Veicolo veicolo : listaVeicoli.listaVeicoli) {
			if (TipoVeicolo.I.equals(veicolo.getTipoVeicolo())) {
				listaImbarcazioni.add((Imbarcazione) veicolo);
			}
		}
	}

	public List<Imbarcazione> getListaImbarcazioni() {
		return listaImbarcazioni;
	}

	public String getImbarcazioneConCaricoMassimo() {

		
		if(listaImbarcazioni.isEmpty()) {
			return "Massimo carico: 0";
		}
		
		int caricoMassimo = 0;
		String nomeImbarcazione = null;
		String targaImbarcazione = null;

		for (Imbarcazione imbarcazione : listaImbarcazioni) {
			String caricoMassimoCorrente = imbarcazione.getMassimoCarico();
			String nomeImbarcazioneCorrente = imbarcazione.getNome();
			String targaImbarcazioneCorrente = imbarcazione.getTarga();

			int caricoMassimoCorrenteInteger = Integer.parseInt(caricoMassimoCorrente);
			if (caricoMassimo < caricoMassimoCorrenteInteger) {
				caricoMassimo = caricoMassimoCorrenteInteger;
				nomeImbarcazione = nomeImbarcazioneCorrente;
				targaImbarcazione = targaImbarcazioneCorrente;
			}
		}

		return "Massimo Carico: " + caricoMassimo + "KG" + " (" + nomeImbarcazione + " - " + targaImbarcazione + ")";
	}
}
