package it.prismaprogetti.academy.statistichemezzi.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;
import it.prismaprogetti.academy.statistichemezzi.UsoVelivolo;


/**
 * classe che contiene tutti i veicoli presi dai file della directory precedentemente ciclata
 * @author alessandro.izzo
 *
 */
public class ListaVeicoli {

	List<Veicolo> listaVeicoli = new ArrayList<>();

	public ListaVeicoli(File[] filesVeicoli) throws Exception {
		super();
		caricaVeicoli(filesVeicoli);

	}

	public List<Veicolo> getListaVeicoli() {
		return listaVeicoli;
	}

	/**
	 * Carica i dati che stanno sui files e li trasforma in oggetti Autovettura\Velivoli\Imbarcazioni
	 * e li aggiungi alla listaVeicoli
	 * @param dati
	 * @param listaDati
	 * @throws Exception
	 */
	private void caricaVeicoli(File[] files) throws Exception {

		for (File file : files) {
			try (FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader)) {

				String veicolo = null;
				while ((veicolo = bufferedReader.readLine()) != null) {
					String[] datiVeicoloArray = veicolo.trim().split("\\|");
					String tipoVeicolo = datiVeicoloArray[0];
					if (tipoVeicolo.equals(TipoVeicolo.A.name())) {
						String targa = datiVeicoloArray[1];
						String modello = datiVeicoloArray[2];
						String cilindrata = datiVeicoloArray[3];
						String kw = datiVeicoloArray[4];
						String posti = datiVeicoloArray[5];
						String dataImmatricolazione = datiVeicoloArray[6];
						listaVeicoli.add(Autovettura.creaAutovettura(TipoVeicolo.A, targa, modello, cilindrata, kw,
								posti, dataImmatricolazione));

					}
					if (tipoVeicolo.equals(TipoVeicolo.V.name())) {
						String codice = datiVeicoloArray[1];
						String modello = datiVeicoloArray[2];
						UsoVelivolo usoVelivolo;
						if (datiVeicoloArray[3].equals("C")) {
							usoVelivolo = UsoVelivolo.C;
						} else {
							usoVelivolo = UsoVelivolo.M;
						}
						String velocitaKH = datiVeicoloArray[4];
						String quotaMassima = datiVeicoloArray[5];
						String dataImmatricolazione = datiVeicoloArray[6];
						listaVeicoli.add(Velivolo.creaVelivolo(TipoVeicolo.V, codice, modello, usoVelivolo, velocitaKH,
								quotaMassima, dataImmatricolazione));
					}

					if (tipoVeicolo.equals(TipoVeicolo.I.name())) {

						String targa = datiVeicoloArray[1];
						String nome = datiVeicoloArray[2];
						String velocitaMassimaKN = datiVeicoloArray[3];
						String caricoMassimo = datiVeicoloArray[4];
						String dataImmatricolazione = datiVeicoloArray[5];
						listaVeicoli.add(Imbarcazione.creaImbarcazione(TipoVeicolo.I, targa, nome, velocitaMassimaKN,
								caricoMassimo, dataImmatricolazione));

					}

				}

			} catch (Exception e) {
				throw new Exception("Qualcosa � andato storto durante la lettura dei File");
			}
		}

	}
}
