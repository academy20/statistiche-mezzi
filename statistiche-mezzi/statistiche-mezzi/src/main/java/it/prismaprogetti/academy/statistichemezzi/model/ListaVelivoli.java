package it.prismaprogetti.academy.statistichemezzi.model;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;

/**
 * classe che contiene la lista di tutti i velivoli presi dalla classe
 * ListaVeicoli
 * 
 * @author alessandro.izzo
 *
 */
public class ListaVelivoli {

	private List<Velivolo> listaVelivoli = new ArrayList<>();

	public ListaVelivoli(ListaVeicoli listaVeicoli) {
		caricaVelivoli(listaVeicoli);
	}

	private void caricaVelivoli(ListaVeicoli listaVeicoli) {

		for (Veicolo veicolo : listaVeicoli.listaVeicoli) {
			if (TipoVeicolo.V.equals(veicolo.getTipoVeicolo())) {
				listaVelivoli.add((Velivolo) veicolo);
			}
		}
	}

	public List<Velivolo> getListaVelivoli() {
		return listaVelivoli;
	}

	public String getVelivoloConVelocitāMaggiore() {

		if (listaVelivoli.isEmpty()) {
			return "Velocitā maggiore: 0";
		}

		int velocitaMaggiore = 0;
		String modelloVelivolo = null;
		String codiceVelivolo = null;

		for (Velivolo velivolo : listaVelivoli) {
			String velocitaMaggioreCorrente = velivolo.getVelocitaKH();
			String modelloVelivoloCorrente = velivolo.getModello();
			String codiceVelivoloCorrente = velivolo.getTarga();

			int velocitaMaggioreCorrenteInteger = Integer.parseInt(velocitaMaggioreCorrente);
			if (velocitaMaggiore < velocitaMaggioreCorrenteInteger) {

				velocitaMaggiore = velocitaMaggioreCorrenteInteger;
				modelloVelivolo = modelloVelivoloCorrente;
				codiceVelivolo = codiceVelivoloCorrente;
			}
		}

		return "Velocitā maggiore: " + velocitaMaggiore + " KH" + " (" + modelloVelivolo + " - " + codiceVelivolo + ")";

	}
}
