package it.prismaprogetti.academy.statistichemezzi.model;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;

/**
 * bean veicolo � la classe padre degli altri
 * veicoli(autovetture,velivoli,imbarcazioni)
 * 
 * @author alessandro.izzo
 *
 */
public class Veicolo {

	private TipoVeicolo tipoVeicolo;
	private String dataImmatricolazione;
	private String targa;

	protected Veicolo(TipoVeicolo tipoVeicolo, String dataImmatricolazione, String targa) {
		super();
		this.tipoVeicolo = tipoVeicolo;
		this.dataImmatricolazione = dataImmatricolazione;
		this.targa = targa;
	}

	public TipoVeicolo getTipoVeicolo() {
		return tipoVeicolo;
	}

	public String getDataImmatricolazione() {
		return dataImmatricolazione;
	}

	public String getTarga() {
		return targa;
	}

	@Override
	public String toString() {
		return "Veicolo [tipoVeicolo=" + tipoVeicolo + ", dataImmatricolazione=" + dataImmatricolazione + ", targa="
				+ targa + "]";
	}

}
