package it.prismaprogetti.academy.statistichemezzi.model;

import it.prismaprogetti.academy.statistichemezzi.TipoVeicolo;
import it.prismaprogetti.academy.statistichemezzi.UsoVelivolo;

/**
 * bean velivolo che estende Veicolo(classe padre)
 * @author alessandro.izzo
 *
 */
public class Velivolo extends Veicolo {

	private TipoVeicolo tipoVeicolo;
	private String codiceTarga;
	private String modello;
	private UsoVelivolo usoVelivolo;
	private String velocitaKH;
	private String quotaMassima;
	private String dataImmatricolazione;

	private Velivolo(TipoVeicolo tipoVeicolo, String codiceTarga, String modello, UsoVelivolo usoVelivolo,
			String velocitaKH, String quotaMassima, String dataImmatricolazione) {
		super(tipoVeicolo, dataImmatricolazione, codiceTarga);
		this.tipoVeicolo = tipoVeicolo;
		this.codiceTarga = codiceTarga;
		this.modello = modello;
		this.usoVelivolo = usoVelivolo;
		this.velocitaKH = velocitaKH;
		this.quotaMassima = quotaMassima;
		this.dataImmatricolazione = dataImmatricolazione;
	}

	public TipoVeicolo getTipoVeicolo() {
		return tipoVeicolo;
	}

	public String getCodiceTarga() {
		return codiceTarga;
	}

	public String getModello() {
		return modello;
	}

	public UsoVelivolo getUsoVelivolo() {
		return usoVelivolo;
	}

	public String getVelocitaKH() {
		return velocitaKH;
	}

	public String getQuotaMassima() {
		return quotaMassima;
	}

	public String getDataImmatricolazione() {
		return dataImmatricolazione;
	}

	
	public static Velivolo creaVelivolo(TipoVeicolo tipoVeicolo, String codiceTarga, String modello,
			UsoVelivolo usoVelivolo, String velocitaKH, String quotaMassima, String dataImmatricolazione) {

		return new Velivolo(tipoVeicolo, codiceTarga, modello, usoVelivolo, velocitaKH, quotaMassima,
				dataImmatricolazione);
	}

	@Override
	public String toString() {
		return "Velivolo [tipoVeicolo=" + tipoVeicolo + ", codiceTarga=" + codiceTarga + ", modello=" + modello
				+ ", usoVelivolo=" + usoVelivolo + ", velocitaKH=" + velocitaKH + ", quotaMassima=" + quotaMassima
				+ ", dataImmatricolazione=" + dataImmatricolazione + "]";
	}
	
	
}
