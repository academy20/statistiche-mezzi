package it.prismaprogetti.academy.statistichemezzi.reader;

import java.io.File;

import it.prismaprogetti.academy.statistichemezzi.error.DecoderReaderException;

public class Reader {

	/**
	 * legge una directory e torna un array dei file all'interno
	 * @param directory
	 * @return
	 * @throws DecoderReaderException
	 */
	public static File[] reatriveFilesFromDirectory(File directory) throws DecoderReaderException {

		if (directory.isDirectory() == false) {
			throw DecoderReaderException.isNotDirectory();

		}
		File[] listFiles = directory.listFiles();

		return listFiles;
	}
}
