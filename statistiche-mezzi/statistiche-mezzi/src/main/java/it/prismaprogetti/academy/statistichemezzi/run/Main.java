package it.prismaprogetti.academy.statistichemezzi.run;

import java.io.File;

import it.prismaprogetti.academy.statistichemezzi.model.ListaAutovetture;
import it.prismaprogetti.academy.statistichemezzi.model.ListaImbarcazioni;
import it.prismaprogetti.academy.statistichemezzi.model.ListaVeicoli;
import it.prismaprogetti.academy.statistichemezzi.model.ListaVelivoli;
import it.prismaprogetti.academy.statistichemezzi.reader.Reader;
import it.prismaprogetti.academy.statistichemezzi.writer.WriterStatistiche;

public class Main {

	public static void main(String[] args) throws Exception {
		

		/*
		 * prendo la directory con i file veicoli-data
		 */
		File directory = new File("C:\\Users\\alessandro.izzo\\Desktop\\output");

		/*
		 * recupero i files dalla directory
		 */
		File[] files = Reader.reatriveFilesFromDirectory(directory);

		/*
		 * carico i dati dei files nella classe ListaVeicoli
		 */
		ListaVeicoli listaVeicoli = new ListaVeicoli(files);

		/*
		 * carico i veicoli nelle rispettive classi in base alla tipologia del veicolo
		 */
		ListaAutovetture listaAutovetture = new ListaAutovetture(listaVeicoli);
		ListaImbarcazioni listaImbarcazioni = new ListaImbarcazioni(listaVeicoli);
		ListaVelivoli listaVelivoli = new ListaVelivoli(listaVeicoli);

		
		/*
		 * scrivo le statistiche su file e le stampo anche su console
		 */
		WriterStatistiche.scriviStatisticheSuFile(files, listaAutovetture, listaVelivoli, listaImbarcazioni,"**********&&*******");
		WriterStatistiche.stampaStatisticheSuConsole(files, listaAutovetture, listaVelivoli, listaImbarcazioni,"*******************");
	}

}
