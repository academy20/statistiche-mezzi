package it.prismaprogetti.academy.statistichemezzi.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import it.prismaprogetti.academy.statistichemezzi.model.ListaAutovetture;
import it.prismaprogetti.academy.statistichemezzi.model.ListaImbarcazioni;
import it.prismaprogetti.academy.statistichemezzi.model.ListaVelivoli;

public class WriterStatistiche {

	/*
	 * file dove andranno le statistiche
	 */
	private static File fileStatistiche = new File(
			"C:\\Users\\alessandro.izzo\\git-statistiche-mezzi\\statistiche-mezzi\\statistiche-mezzi\\src\\main\\resources\\veicoli-statistiche.dat");

	public static void scriviStatisticheSuFile(File[] filesVeicoli, ListaAutovetture listaAutovetture,
			ListaVelivoli listaVelivoli, ListaImbarcazioni listaImbarcazioni, String marcatoreFineRiga) {

		try (FileWriter fileWriter = new FileWriter(fileStatistiche);
				BufferedWriter buffWriter = new BufferedWriter(fileWriter)) {

			/*
			 * prima cosa mi serve la stringa del file e devo prendere la data di arrivo
			 */
			String dateArrivo = "";
			for (File file : filesVeicoli) {
				String nomeFile = file.getName().substring(8, 16);
				dateArrivo = dateArrivo + "," + nomeFile;
			}

			String dateArrivoPulita = dateArrivo.trim().substring(1);
			buffWriter.write("DATE ARRIVO: " + dateArrivoPulita + "\n");

			/*
			 * ora mi serve la quantita per tipo veicoli
			 */
			int numeroAutovetture = listaAutovetture.getListaAutovetture().size();
			int numeroVelivoli = listaVelivoli.getListaVelivoli().size();
			int numeroImbarcazioni = listaImbarcazioni.getListaImbarcazioni().size();

			/*
			 * scrivo su file le informazioni
			 */
			buffWriter.write("Autovetture: " + numeroAutovetture + "\n");
			buffWriter.write("Velivoli: " + numeroVelivoli + "\n");
			buffWriter.write("Imbarcazioni: " + numeroImbarcazioni + "\n");
			buffWriter.write(marcatoreFineRiga + "\n");
			buffWriter.write("AUTOMOBILI" + "\n");
			buffWriter.write(marcatoreFineRiga + "\n");
			buffWriter.write(listaAutovetture.getImmatricolazionePiuRecente() + "\n");
			buffWriter.write(marcatoreFineRiga + "\n");
			buffWriter.write("VELIVOLI" + "\n");
			buffWriter.write(marcatoreFineRiga + "\n");
			buffWriter.write(listaVelivoli.getVelivoloConVelocitāMaggiore() + "\n");
			buffWriter.write(marcatoreFineRiga + "\n");
			buffWriter.write("IMBARCAZIONI" + "\n");
			buffWriter.write(marcatoreFineRiga + "\n");
			buffWriter.write(listaImbarcazioni.getImbarcazioneConCaricoMassimo());

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void stampaStatisticheSuConsole(File[] filesVeicoli, ListaAutovetture listaAutovetture,
			ListaVelivoli listaVelivoli, ListaImbarcazioni listaImbarcazioni, String marcatoreFineRiga) {

		/*
		 * prima cosa mi serve la stringa del file e devo prendere la data di arrivo
		 */
		String dateArrivo = "";
		for (File file : filesVeicoli) {
			String nomeFile = file.getName().substring(8, 16);
			dateArrivo = dateArrivo + "," + nomeFile;
		}

		String dateArrivoPulita = dateArrivo.trim().substring(1);
		System.out.println(("DATE ARRIVO: " + dateArrivoPulita + "\n"));

		/*
		 * ora mi serve la quantita per tipo veicoli
		 */
		int numeroAutovetture = listaAutovetture.getListaAutovetture().size();
		int numeroVelivoli = listaVelivoli.getListaVelivoli().size();
		int numeroImbarcazioni = listaImbarcazioni.getListaImbarcazioni().size();

		/*
		 * scrivo su Console le informazioni
		 */
		System.out.println(("Autovetture: " + numeroAutovetture + "\n"));
		System.out.println(("Velivoli: " + numeroVelivoli + "\n"));
		System.out.println(("Imbarcazioni: " + numeroImbarcazioni + "\n"));
		System.out.println((marcatoreFineRiga + "\n"));
		System.out.println(("AUTOMOBILI" + "\n"));
		System.out.println((marcatoreFineRiga + "\n"));
		System.out.println((listaAutovetture.getImmatricolazionePiuRecente() + "\n"));
		System.out.println((marcatoreFineRiga + "\n"));
		System.out.println(("VELIVOLI" + "\n"));
		System.out.println((marcatoreFineRiga + "\n"));
		System.out.println((listaVelivoli.getVelivoloConVelocitāMaggiore() + "\n"));
		System.out.println((marcatoreFineRiga + "\n"));
		System.out.println(("IMBARCAZIONI" + "\n"));
		System.out.println((marcatoreFineRiga + "\n"));
		System.out.println((listaImbarcazioni.getImbarcazioneConCaricoMassimo()));

	}
}
